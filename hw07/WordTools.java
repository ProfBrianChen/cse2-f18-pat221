
// Paula Torrebiarte
// CSE 2
// HW 07
import java.util.Scanner;
public class WordTools{
  public static String sampleText(){// method ask for text
    Scanner myScanner = new Scanner(System.in);
   System.out.print("Enter Sample text: ");
   String str = myScanner.nextLine();
      System.out.println( "You entered:" + str); // promp same text
    return str;
 
      
  }
  public static char printMenu(){
  //method menu
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Menu");
     System.out.println("c - number of nonwhitespace characters");
     System.out.println("w - number of words");
     System.out.println("f - Find Text");
     System.out.println("r - Replace all !'s");
      System.out.println("s - Shorten spaces");
     System.out.println("q - quit");
     System.out.print("Choose an option: ");
    char menuOption = myScanner.next().charAt(0);
    return menuOption;
    
  } 
  public static int getNumOfNonWSCharacters(String str){
  // method count characters
  int count =  str.replace(" ","").length(); 
    System.out.println(" Number Non whiteSpace characters:" + count);
  return count;
  }
  public static int getNumOfWords(String str){
   // method count number of word
    int words=0;
    int length=str.length()-1;
    for (int i = 0; i <=length;i++){
      if(Character.isLetter(str.charAt(i))){
        words++;
        for (i=0; i<=length; i++){
          if(str.charAt(i)==' '){
            words++;
        }
      }
    }
    }
    System.out.println(" Number of words:"+words); 
    return words;
  }
  public static int findText(String str){
    // method find string inside texs
    int count = 0;
    Scanner myScanner = new Scanner(System.in);
    System.out.println(" Enter a word or phrase to be found:");
    String searchedWord = myScanner.nextLine();
    for (int i = 0; i< str.length();i++){
      if (str.contains(searchedWord)){ // if conatain word increase count
        count++;
      }
    }
    System.out.println(searchedWord + " instances: "+ count);
    return count;
  } 
  public static String replaceExclamation(String str){
    String replacement = str.replace("!",".");
    System.out.println("Edited Text:" + replacement);
    return replacement;
  } // method replace exclamation with .
  public static String shortenSpace(String str){
    String shortenText = str.replaceAll("\\s+", " " );
    System.out.println("Edited Text: " + shortenText);
  return shortenText;
  } // method replace double space w/ single
  public static void main(String[] args){
   
    String str = sampleText(); // ask for text
    char menu = printMenu(); // printmenu
   //declare variable
    int countNonWS = 0;
    int wordCount = 0;
    int wordSearch = 0;
    String edited;
    String shorten;
  
  while (menu != 'q'){ // run only if not q
    switch (menu){
      case 'c':
         countNonWS = getNumOfNonWSCharacters(str);
      
        break;
      case 'w':
        wordCount = getNumOfWords(str);
       
        break;
      case 'f':
         wordSearch = findText(str);
      
        break;
      case 'r':
         edited = replaceExclamation(str);
       
        break;
      case 's': 
         shorten = shortenSpace(str);
        break;
  } // switch if any of the characters of the menu
    menu = printMenu();// resent menu variable
  
  }
    
   
    
    
 
  }// end of main method
}// end main class