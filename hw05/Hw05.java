
import java.util.Scanner;

public class Hw05{ //start class
  public static void main (String[] args){ // start of main method
  Scanner myScanner = new Scanner(System.in); //declare an instance of the Scanner and call the scanner constructor  
 
  
  int probFour = 0;
  int probThree = 0;
  int probTwoPair = 0;
  int probOnePair = 0;  
  
    
   System.out.print("How many times should the program generate hands? "); // output original cost of check
    int trials = myScanner.nextInt(); // accepts user input
    // declare suits based on numbers 
   
  int count = 0;
  int pair;
  int randomNumber1;
  int randomNumber2;
    int randomNumber3;
    int randomNumber4;
    int randomNumber5;
  while ( count < trials){
    pair = 0;
  randomNumber1 = (int)(Math.random()*52)+1; // random number 1 
  do{
   randomNumber2 = (int)(Math.random()*52)+1; // random number 2 
  }while(randomNumber2==randomNumber1);
  do{
  randomNumber3 = (int)(Math.random()*52)+1; // random number 3 
  }while(randomNumber3==randomNumber1 || randomNumber3==randomNumber2);
  do{
  randomNumber4 = (int)(Math.random()*52)+1; // random number 4
  }while(randomNumber4==randomNumber3 || randomNumber4==randomNumber2 || randomNumber4==randomNumber1);
  do{
  randomNumber5 = (int)(Math.random()*52)+1; // random number 5  
  }while(randomNumber5==randomNumber4 || randomNumber5==randomNumber3 || randomNumber5==randomNumber2 || randomNumber5==randomNumber1);
  
  int remainder1 = randomNumber1%13;
  int remainder2 = randomNumber2%13;
  int remainder3 = randomNumber3%13;
  int remainder4 = randomNumber4%13;
  int remainder5 = randomNumber5%13;
if (remainder1==remainder2) {
    pair++;
}
 if(remainder1==remainder3){
   pair++;
 }
 if(remainder1==remainder4){
   pair++;
 }   
 if(remainder1==remainder5){
  pair++;
}   
  if(remainder2==remainder3){
    pair++;
}
  if(remainder2==remainder4){
   pair++ ;
 }
  if(remainder2==remainder5){
   pair++; 
 }   
  if(remainder3==remainder4){
  pair++;
}
   if(remainder3==remainder5){
  pair++;
}  
  if(remainder4==remainder5){
      pair++;
}
    
    if (pair == 1){ 
      probOnePair++;
    }
    if (pair == 2 || pair == 4){ //pair = 4 when full house
      probTwoPair++;
    }
    if (pair == 3 || pair == 4){ // pair = 3 when three of a kind, pair = 4 when full house
      probThree++;
    }
    if (pair == 6){ // pair = 6 when four of a kind
      probFour++;
    }
   
    count++;
 }
    
System.out.println("The number of loops:" + trials);
System.out.println("The probability of Four-of-a-kind: "+ (double)probFour/(double) trials); 
System.out.println("The probability of Three-of-a-kind:" + (double)probThree/(double) trials);
System.out.println("The probability of Two-pair: " + (double)probTwoPair/(double)trials);
System.out.println("The probability of One-pair: " + (double)probOnePair/(double)trials);

  } // end of main method 
  
} // end class