

// Paula Torrebiarte
// CSE 2
// Pattern A

import java.util.Scanner;
public class PatternA{
  public static void main (String[] args){
  Scanner myScanner = new Scanner(System.in);
 
  int i,j; // declare rows and colums
    System.out.print(" Integer between 1 and 10: "); // input
    int length = myScanner.nextInt(); // save input
   
    while(length>9 || length<2) { // check if int is in the range
      System.out.println(" Error! ");  
      System.out.print(" Integer between 1 and 10: ");
      length = myScanner.nextInt(); 
    } 
   
    // for loop actual pattern 
    for ( i=1; i<=length; i ++){ /// number of rows
      
      for ( j=1; j<=i; j++){ //columsn
        System.out.print( j +" ");
      }
      System.out.println();
    }
    
    
  } // end of main method
}// end of class