// Paula Torrebiarte
// CSE 2
// Pattern D

import java.util.Scanner;
public class PatternD{
  public static void main (String[] args){
  Scanner myScanner = new Scanner(System.in);
    
  int i,j;
   System.out.print(" Integer between 1 and 10: "); // input
    int length = myScanner.nextInt(); // save input
   
    while(length>9 || length<2) { // check if int is in the range
      System.out.println(" Error! ");  
      System.out.print(" Integer between 1 and 10: ");
      length = myScanner.nextInt(); 
    } 
    // loop for pattern 
    for ( i=1; i<=length ; i++){ /// number of rows
 
      for ( j=length-i+1; j>=1; j--){ //columns 
        System.out.print ( j +" ");
      }
      System.out.println();
    }
    
  } // end of main method
  }// end of class
