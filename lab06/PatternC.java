// Paula Torrebiarte
// CSE 2
// Pattern c

import java.util.Scanner;
public class PatternC{
  public static void main (String[] args){
  Scanner myScanner = new Scanner(System.in);
  int i,j,k,a;
    
  System.out.print(" Integer between 1 and 10: "); // input
    int length = myScanner.nextInt(); // save input
   
    while(length>9 || length<2) { // check if int is in the range
      System.out.println(" Error! ");  
      System.out.print(" Integer between 1 and 10: ");
      length = myScanner.nextInt(); 
    } 
    // loop 
    for ( i=1; i<=length; i ++){ /// number of rows
      for (k = length-1; k>=i; k--) // spaces 
         System.out.print(" ");
      for ( j=i; j>=1; j--){ // columns
         System.out.print(j+"");
      }
      System.out.println();
    }
    
  } // end of main method
}// end of class