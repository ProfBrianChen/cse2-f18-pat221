
/// Paula Torrebiarte
/// 09/11/18
// Arithmetic

public class Arithmetic{

public static void main(String[] args){

  // input data
  //Number of pairs of pants
  int numPants=3;
  //Cost per pair of pants
  double pantsPrice=34.98;
  //total cost of pants 
  double totalCostPants;
  //sales tax on pants
  double salesTaxPants;

  //Number of sweatshirts
  int numShirts=2;
  //Cost per shirt
  double shirtPrice=24.99;
  //total cost of shirts 
  double totalCostShirts;
  //sales tax on shirts
  double salesTaxShirts;

  //Number of belts
  int numBelts=1;
  //cost per belt
  double beltsPrice = 33.99;
  //total cost of belts
  double totalCostBelts;
  //sales tax on Belts
  double salesTaxBelts;

  //the tax rate
  double paSalesTax = 0.06;
  // total cost before tax
  double totalCost;
  // total Sales tax
  double totalSalesTax;
  // total paid in transaction with tax
  double totalPaid;

  //calculations total cost per item 
  totalCostPants=numPants*pantsPrice; //total cost pants without tax
  totalCostShirts=numShirts*shirtPrice; //total cost shirts without tax
  totalCostBelts=numBelts*beltsPrice; //total cost pants without tax

  //output total cost per item 
  System.out.println("The total cost of pants without tax is $" + totalCostPants); 
  System.out.println("The total cost of sweatshirts without tax is $" + totalCostShirts);
  System.out.println("The total cost of belts without tax is $" + totalCostBelts);

  //calculations sales tax per item 
  salesTaxPants=totalCostPants*paSalesTax*100; // sales tax on pants
  salesTaxShirts=totalCostShirts*paSalesTax*100; // sales tax on shirts
  salesTaxBelts=totalCostBelts*paSalesTax*100; // sales tax on belts

  // convert  sales tax of pants to 2 decimals
  int salesTaxPantsInt = (int)salesTaxPants;
  double salesTaxPantsIntF=salesTaxPantsInt/100.00  ;
  // convert  sales tax of shirts to 2 decimals
  int salesTaxShirtsInt = (int)salesTaxShirts;
  double salesTaxShirtsIntF=salesTaxShirtsInt/100.00  ;
  // convert  sales tax of belts to 2 decimals
  int salesTaxBeltsInt = (int)salesTaxBelts;
  double salesTaxBeltsIntF=salesTaxBeltsInt/100.00  ;

  // output sales tax per item 
  System.out.println("The sales tax on pants is $" + salesTaxPantsIntF); 
  System.out.println("The sales tax on sweatshirts is $" + salesTaxShirtsIntF); 
  System.out.println("The sales tax on belts is $" + salesTaxBeltsIntF); 

  // total cost 
  totalCost=totalCostPants+totalCostShirts+totalCostBelts; //calculation
  System.out.println("The total cost of purchase before tax is $"+totalCost); //output

  // total sales tax
  totalSalesTax=salesTaxPantsIntF+salesTaxShirtsIntF+salesTaxBeltsIntF; //calculation
  System.out.println("The total sales tax is $"+totalSalesTax); //output

  // total paid for transaction
  totalPaid=(totalCost+totalSalesTax)*100; // calculation
  int totalPaidInt = (int)totalPaid;
  double totalPaidIntF=totalPaidInt/100.00  ;
  System.out.println("The total paid for the transaction is $" +totalPaidIntF);//output

} // end of main method
} //end of class