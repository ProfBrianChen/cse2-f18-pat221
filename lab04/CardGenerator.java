//
// Paula Torrebiarte
// CSE 2
// Random Card Generator

public class CardGenerator{ //start class
  public static void main (String[] args){ // start of main method
    
  int randomNumber = (int)(Math.random()*51)+1; // random number generator
  String suit; // set variable 
  
    
 
    // declare suits based on numbers
    if (randomNumber<=13){
      suit = "diamonds"; // if less that 13 diamond
    }
    else if (randomNumber <=26){ // from 14 to 26 clubs
      suit = "clubs";
    }
    else if (randomNumber<=40){ // from 27 to 40 heart
      suit = "hearts"; 
    }
     else {
       suit = "spades"; // from 41 to 52 spades
     }
    // declare values based on multipes
    switch (randomNumber) {
    // all values are ace
    case 1:
    case 14:
    case 27:
    case 40:
    System.out.println("Your card is an Ace of " +suit); // print statement based on multiple
    break;
    // values are 2 
    case 2:
    case 15:
    case 28:
    case 41:
    System.out.println("Your card is a 2 of " +suit); // print statement based on multiple
    break;
    // values are 3
    case 3:
    case 16:
    case 29:
    case 42:
    System.out.println("Your card is a 3 of " +suit); // print statement based on multiple
    break;
    // values are 4
    case 4:
    case 17:
    case 30:
    case 43:
    System.out.println("Your card is a 4 of " +suit); // print statement based on multiple
    break;
    // values are 5 
    case 5:
    case 18:
    case 31:
    case 44:
    System.out.println("Your card is a 5 of " +suit); // print statement based on multiple
    break;
    // values are 6   
    case 6:
    case 19:
    case 32:
    case 45:
    System.out.println("Your card is a 6 of " +suit); // print statement based on multiple
    break;
    // values are 7    
    case 7:
    case 20:
    case 33:
    case 46:
    System.out.println("Your card is a 7 of " +suit); // print statement based on multiple
    break;
    // values are 8
    case 8:
    case 21:
    case 34:
    case 47:
    System.out.println("Your card is a 8 of " +suit); // print statement based on multiple
    break;
    // values are 9
    case 9:
    case 22:
    case 35:
    case 48:
    System.out.println("Your card is a 9 of " +suit); // print statement based on multiple
    break;
    // values are 10
    case 10:
    case 23:
    case 36:
    case 49:
    System.out.println("Your card is a 10 of " +suit); // print statement based on multiple
    break;
    // values are Jack
    case 11:
    case 24:
    case 37:
    case 50:
    System.out.println("Your card is a Jack of " +suit); // print statement based on multiple
    break;
    // values are Queen
    case 12:
    case 25:
    case 38:
    case 51:
    System.out.println("Your card is a Queen of " +suit); // print statement based on multiple
    break;
    // values are King   
    case 13:
    case 26:
    case 39:
    case 52:
    System.out.println("Your card is a King of " +suit);// print statement based on multiple
    break;
    } // end of switch statement

    
  } // end of main method 
  
} // end class