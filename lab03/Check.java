//
// Paula Torrebiarte
// CSE 2
// Lab 03 - Check
//
// This will determine how much each person needs to pay of the check

import java.util.Scanner;
public class Check{
  // main methos used in every Java program
  public static void main (String[] args){
    
    Scanner myScanner = new Scanner(System.in); //declare an instance of the Scanner and call the scanner constructor

    System.out.print("Enter the original cost of the check in the form xx.xx: "); // output original cost of check
    double checkCost = myScanner.nextDouble(); // accepts user input

    System.out.print("Enter the percentage tip that you whish to pay as a whole number ( in the form xx ): " ); // input percetage tip
    double tipPercent = myScanner.nextDouble(); // store input
    tipPercent /= 100; // convert percentage into decimal value

    System.out.print("Enter the number of people who went out to dinner: "); //input how many people went to dinner
    int numPeople = myScanner.nextInt(); // store input (how many people went to dinner)
    
    // print out the output
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; //for storing digits
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars=(int)costPerPerson; 
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

  } // end of main method
}// end of class