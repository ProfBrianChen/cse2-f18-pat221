// Paula Torrebiarte
// 25/09/18
// CSE 2
// Ecrypted X using nested loops
// see pattern as plot
import java.util.Scanner;
public class EncryptedX{
  public static void main ( String[] args){
    Scanner myScanner = new Scanner(System.in);
     System.out.print(" Integer between 0 and 100: "); // ask for input
      int size = myScanner.nextInt(); // save input
    
    // check input is in range
    while( size > 99 || size < 1){ // range 
      System.out.println( " ERROR "); // state it is an error 
      System.out.print( "Integer between 0 and 100:"); // ask again
      size = myScanner.nextInt(); // save the int again 
    }// end while statement 
     
    // pattern 
    for (int row = 0; row < size; row++){ // rows 
      for ( int column = 0; column < size; column++){ // columns 
        // if row and column are the same print space -- > (1,1)  
        // set first diagnal 
        if ( row == column){ 
            System.out.print(" ");
          }
        // if row + column = size - 1 --> eg. if size = 5 & (3, 1) = 5 - 1
        // other diagnal 
          else if ( row + column == size - 1 ){
            System.out.print(" ");
          }
        // other is just *
          else {
            System.out.print("*");
          }
      } // of second loop
      System.out.println();
    }// end first for loop 
    
  } // end of main method
}//end of class 