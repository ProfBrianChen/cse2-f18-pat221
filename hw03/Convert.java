// Paula Torrebiarte
// 18/09/18
// CSE 2
// Convert
//  In this program we will convert the quantity of rain into cubic miles 

import java.util.Scanner; // import scanner
public class Convert{ //open class
  public static void main (String[] args){ //open main method
    Scanner myScanner = new Scanner(System.in); //declare an instance of the Scanner and call the scanner constructor
   
      System.out.print("Enter the affected area in acres: "); //output acres affected
        double affectedArea = myScanner.nextDouble(); //accepts input
      
      System.out.print("Enter the rainfall in the affecte area in inches: "); //output amount of rain 
        double amountRain= myScanner.nextDouble(); //accepts input
    
   // calculations 
   double acres = 43560;// 1 acre is 43560 sq ft 
   double inchesCubed=Math.pow(12,3);  // 12 inches cubed 
   double cubicInches=acres*inchesCubed/12;//  Acres times * inches cubed divided by 12 as 12 inches in a feet (conversion factor)
   double gallons = cubicInches * 0.004329; // convert to gallons
   double totalGallons = amountRain*affectedArea*gallons; // multiply by total amount of acres and inches of rain
   double cubicMiles = totalGallons * 0.000000000001; // conversion factor from gallons to cubic miles
    
    // output 
    System.out.println(cubicMiles + " cubic miles"); // number of cubic miles
    
  }// end of main method
} // end of class 