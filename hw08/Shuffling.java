import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 
  // print array 
  public static void printArray (String list[]){
    
   for (int i= 0;  i<list.length; i++){ // run loop for the lenght of arrat
     System.out.print(list[i]+" "); //print
   }
    System.out.println();
  }
  public static String[] shuffle ( String list[]){ // shuffle deck of cards
    Random randomNum = new Random(); 
    String temp =" ";
 
    for(int i=0; i<52; i++){ //shuffle 52 tiems
      int randIndex = randomNum.nextInt(51);
      temp = list[0]; // place of zero
      list[0] = list[randIndex]; 
      list[randIndex] = temp;// reset string
      
     
  }
    System.out.println( "Shuffle:" );
    return list; //save list
  }
  public static String[] getHand( String list[], int index, int numCards){ // get hand based on number of cards
   String hand[] = new String [numCards];
   int count = 0;
    for( int i = index; i> (index-numCards); index--);{//run for times of numCards
      hand[count++] = list[index]; //save hand is equal to list 
   }
    System.out.println( "Hand: ");
    return hand;// save hand
  }

  public static void main(String[] args) { // given by Prof.Chen
  Scanner scan = new Scanner(System.in); 
  
    
    
  //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"};    
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
  String[] cards = new String[52]; 
  String[] hand = new String[5]; 
    
  int numCards = 5; 
  int again = 1; 
  int index = 51;
    
  for (int i=0; i<52; i++){ 
    cards[i]=rankNames[i%13]+suitNames[i/13]; 
    System.out.print(cards[i]+" "); 
  } 
    
  System.out.println();
  printArray(cards); 
  shuffle(cards); 
  printArray(cards); 
    
 while(again == 1){ 
   hand = getHand(cards,index,numCards); 
    printArray(hand);
    index = index - numCards;
    System.out.println("Enter a 1 if you want another hand drawn"); 
    again = scan.nextInt(); 
  } 
    
 } // end main method
} // end main class
