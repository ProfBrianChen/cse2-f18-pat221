// Paula Torrebiarte
// Oct 25 
// CSE 2

import java.util.Scanner;
import java.util.Random;
public class Methods{
public static String Adjectives (){
  Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
  String adj = "";
  switch (randomInt){
    case 0:
      adj = "briliant";
    case 1:
      adj = "absolute";
        break;
    case 2:
      adj = "aggresive";
        break;
    case 3:
      adj = "alarmed";
        break;
    case 4:
      adj = "courageous";
        break;
    case 5:
      adj = "dangerous";
        break;
    case 6:
      adj = "electric";
        break;
    case 7:
      adj = "elegant";
        break;
    case 8:
      adj = "haunting";
        break;
    case 9:  
      adj = "hilarious";
        break;
        
  }
  return (String) adj;
}
public static String NounSubject (){
  Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
  String nounS = "";
  switch (randomInt){
    case 0:
      nounS = "dolphine";
    case 1:
      nounS = "girl";
        break;
    case 2:
      nounS = "tree";
        break;
    case 3:
      nounS = "car";
        break;
    case 4:
      nounS = "boy";
        break;
    case 5:
      nounS = "mom";
        break;
    case 6:
      nounS = "dad";
        break;
    case 7:
      nounS = "hair";
        break;
    case 8:
      nounS = "student";
        break;
    case 9:  
      nounS = "plant";
        break;
        
  }
  return (String) nounS;
}
public static String Verbs (){
  Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
  String verb = "";
  switch (randomInt){
    case 0:
      verb = "cared";
    case 1:
      verb = "played";
        break;
    case 2:
      verb = "cooked";
        break;
    case 3:
      verb = "rained";
        break;
    case 4:
     verb = "watches";
        break;
    case 5:
    verb = "wanted";
        break;
    case 6:
      verb = "went";
        break;
    case 7:
      verb = "ate";
        break;
    case 8:
      verb = "danced";
        break;
    case 9:  
      verb = "stole";
        break;
        
  }
  return (String) verb;
}
public static String NounObject (){
  Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
  String nounO = "";
  switch (randomInt){
    case 0:
      nounO = "cake";
    case 1:
      nounO = "park";
        break;
    case 2:
      nounO = "grammar";
        break;
    case 3:
      nounO = "building";
        break;
    case 4:
     nounO = "milk";
        break;
    case 5:
    nounO = "train";
        break;
    case 6:
      nounO = "car";
        break;
    case 7:
      nounO = "wolf";
        break;
    case 8:
      nounO = "monkeys";
        break;
    case 9:  
      nounO = "plane";
        break;
        
  }
  return (String) nounO;
}
  public static String Phase2(){
   String noun = NounSubject();
    System.out.println("The" + " " + Adjectives() + " " + noun + " " + Verbs() + " " + "the" + " " + Adjectives() + " " + NounObject());
  
  
  Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(2);
  if (randomInt == 0){
   System.out.println("This " + noun + " " + Adjectives() + " " +  Verbs() +" to " + Verbs()+ " " + NounObject()); 
  }
  else{
    System.out.println( "It used "+ NounObject()+" to "+ Verbs() + " " + NounObject() +" at "+ Adjectives() + " " + NounObject() );
  }
   System.out.println(" That "+noun+ " " + Verbs()+ " " + Adjectives());
    return noun;
}
  public static void main (String[] args){
     Scanner myScanner = new Scanner(System.in);
    System.out.println("The" + " " + Adjectives() + " " + NounSubject() + " " + Verbs() + " " + "the" + " " + Adjectives() + " " + NounObject());
  System.out.print("Do you want another sentence (a for yes):");
   char response = myScanner.next().charAt(0);
    while ( response == 'a'){
      System.out.println("The" + " " + Adjectives() + " " + NounSubject() + " " + Verbs() + " " + "the" + " " + Adjectives() + " " + NounObject());
  System.out.print("Do you want another sentence :");
    response = myScanner.next().charAt(0);
    }
    Phase2();
    
  } // end of method
}// end of class