
// Paula Torrebiarte
// 25/09/18
//CSE 2
// Craps Using switch statements
  
import java.util.Scanner; // import scanner
public class CrapsSwitch{ // open class
  public static void main ( String[] args){ // open main method
    Scanner myScanner = new Scanner(System.in); //declare an instance of the Scanner and call the scanner constructor
      System.out.print(" Do you want to cast a dice or evaluate (1 or 2)? "); // ask if cast or evaluate 
     int type = myScanner.nextInt();// save input
 
    switch (type){ // open main switch 
     // switch statment if they choose to cast
      case 1:
        int randomNumberOne = (int)(Math.random()*5)+1; // random number generator  (dice 1)
        int randomNumberTwo = (int)(Math.random()*5)+1; // random number generator  (dice 2)
      // switch if one dice is 1 and the slang for all the other options
       switch (randomNumberTwo){
         case 1:
           switch (randomNumberOne){
             case 1: 
               System.out.println(" Snake Eyes ");
             break;
             case 2:
              System.out.println(" Ace Deuce ");
               break;
             case 3:
               System.out.println(" Easy Four ");
               break;
             case 4:
               System.out.println(" Fever Five ");
               break;
             case 5:
               System.out.println(" Easy Six ");
               break;
             case 6:
               System.out.println(" Seven Out ");
            } 
           break;
            // switch if one dice is 2 and the slang for all the other options
         case 2:
           switch (randomNumberOne){
           case 1: 
               System.out.println(" Snake Eyes ");
             break;
             case 2:
              System.out.println(" Hard Four ");
               break;
             case 3:
               System.out.println(" Fever Five ");
               break;
             case 4:
               System.out.println(" Easy Six ");
               break;
             case 5:
               System.out.println(" Seven Out ");
               break;
             case 6:
               System.out.println(" Easy Eight ");
       }
           break;
            // switch if one dice is 3 and the slang for all the other options
            case 3:
           switch (randomNumberOne){
           case 1: 
               System.out.println(" Easy Four ");
             break;
             case 2:
              System.out.println(" Fever Five ");
               break;
             case 3:
               System.out.println(" Hard Six ");
               break;
             case 4:
               System.out.println(" Seven Out ");
               break;
             case 5:
               System.out.println(" Easy Eight ");
               break;
             case 6:
               System.out.println(" Nine ");
       }
           break;
            // switch if one dice is 4 and the slang for all the other options
            case 4:
           switch (randomNumberOne){
           case 1: 
               System.out.println(" Fever five ");
             break;
             case 2:
              System.out.println(" Easy Six ");
               break;
             case 3:
               System.out.println(" Seven Out ");
               break;
             case 4:
               System.out.println(" Hard Eight ");
               break;
             case 5:
               System.out.println(" Nine ");
               break;
             case 6:
               System.out.println(" Easy Ten ");
       }
           break;
            // switch if one dice is 5 and the slang for all the other options
          case 5:
           switch (randomNumberOne){
           case 1: 
               System.out.println(" Easy Six ");
             break;
             case 2:
              System.out.println(" Seven Out ");
               break;
             case 3:
               System.out.println(" Easy Eight ");
               break;
             case 4:
               System.out.println(" Nine ");
               break;
             case 5:
               System.out.println(" Hard Ten ");
               break;
             case 6:
               System.out.println(" Yo-Leven ");
       }
           break;
            // switch if one dice is 6 and the slang for all the other options
          case 6:
           switch (randomNumberOne){
           case 1: 
               System.out.println(" Seven Out");
             break;
             case 2:
              System.out.println(" Easy Eight ");
               break;
             case 3:
               System.out.println(" Nine ");
               break;
             case 4:
               System.out.println(" Easy Ten ");
               break;
             case 5:
               System.out.println(" Yo-Leven ");
               break;
             case 6:
               System.out.println(" Boxcars ");
       } 
    } // end switch if choose cast dice
        break;
        // if they choose 2 numbers to be evaluted
      case 2:
        System.out.print(" First number: "); //  input first number
        int numberOne = myScanner.nextInt();// save input (first number)
        System.out.print(" Second number: "); // input second number
        int numberTwo = myScanner.nextInt(); // save input second number
        // switch if one number is 1 and the slang for all the other options
        switch (numberTwo){
         case 1:
           
           switch (numberOne){
             case 1: 
               System.out.println(" Snake Eyes ");
             break;
             case 2:
              System.out.println(" Ace Deuce ");
               break;
             case 3:
               System.out.println(" Easy Four ");
               break;
             case 4:
               System.out.println(" Fever Five ");
               break;
             case 5:
               System.out.println(" Easy Six ");
               break;
             case 6:
               System.out.println(" Seven Out ");
            } 
           break;
              // switch if one number is 2 and the slang for all the other options
         case 2:
           switch (numberOne){
           case 1: 
               System.out.println(" Snake Eyes ");
             break;
             case 2:
              System.out.println(" Hard Four ");
               break;
             case 3:
               System.out.println(" Fever Five ");
               break;
             case 4:
               System.out.println(" Easy Six ");
               break;
             case 5:
               System.out.println(" Seven Out ");
               break;
             case 6:
               System.out.println(" Easy Eight ");
       }
           break;
              // switch if one number is 3 and the slang for all the other options
            case 3:
           switch (numberOne){
           case 1: 
               System.out.println(" Easy Four ");
             break;
             case 2:
              System.out.println(" Fever Five ");
               break;
             case 3:
               System.out.println(" Hard Six ");
               break;
             case 4:
               System.out.println(" Seven Out ");
               break;
             case 5:
               System.out.println(" Easy Eight ");
               break;
             case 6:
               System.out.println(" Nine ");
       }
           break;
              // switch if one number is 4 and the slang for all the other options
            case 4:
           switch (numberOne){
           case 1: 
               System.out.println(" Fever five ");
             break;
             case 2:
              System.out.println(" Easy Six ");
               break;
             case 3:
               System.out.println(" Seven Out ");
               break;
             case 4:
               System.out.println(" Hard Eight ");
               break;
             case 5:
               System.out.println(" Nine ");
               break;
             case 6:
               System.out.println(" Easy Ten ");
       }
           break;
              // switch if one number is 5 and the slang for all combinations
          case 5:
           switch (numberOne){
           case 1: 
               System.out.println(" Easy Six ");
             break;
             case 2:
              System.out.println(" Seven Out ");
               break;
             case 3:
               System.out.println(" Easy Eight ");
               break;
             case 4:
               System.out.println(" Nine ");
               break;
             case 5:
               System.out.println(" Hard Ten ");
               break;
             case 6:
               System.out.println(" Yo-Leven ");
       }
           break;
              // switch if one number is 6 and the slang for all the combinatios
          case 6:
           switch (numberOne){
           case 1: 
               System.out.println(" Seven Out");
             break;
             case 2:
              System.out.println(" Easy Eight ");
               break;
             case 3:
               System.out.println(" Nine ");
               break;
             case 4:
               System.out.println(" Easy Ten ");
               break;
             case 5:
               System.out.println(" Yo-Leven ");
               break;
             case 6:
               System.out.println(" Boxcars ");
       } 
    } // end switch for evaluate  
    } // end main switch
  }// end of main method
}// end of class