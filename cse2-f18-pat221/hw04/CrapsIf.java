
// Paula Torrebiarte
// 25/09/18
// CSE 2
// Craps Using If statements
// the sum of the die determine the name 
import java.util.Scanner;
public class CrapsIf{
  public static void main ( String[] args){
    Scanner myScanner = new Scanner(System.in); //declare an instance of the Scanner and call the scanner constructor
      System.out.print(" Do you want to cast a dice or evaluate (1 or 2)? "); // ask for what they want 
      double type = myScanner.nextDouble(); // save input 
       
        if (type==1){ // if they decide cast
          int randomNumberOne = (int)(Math.random()*5)+1; // random number generator
          int randomNumberTwo = (int)(Math.random()*5)+1; // random number generator

          if (randomNumberOne + randomNumberTwo == 2){ // sum is two 
          System.out.println("Snake Eyes"); // slang for two 
          }// end if 
          else if (randomNumberOne + randomNumberTwo == 3){ // if sum is three
              System.out.println("Ace Deuce"); // slang for three
           } // end else if
          else if (randomNumberOne + randomNumberTwo == 4){ // if sum 4
             if ( randomNumberOne == randomNumberTwo){ // if both ramdom number are the same
                System.out.println("Hard Four"); // slang for 2 twos
                }
             else {
              System.out.println("Easy Four"); // slang for four when not 2+2
              }
           }
           else if (randomNumberOne + randomNumberTwo == 5){ // if sum is 5
               System.out.println("Fever Five"); // slang for 5 
          }
           else if (randomNumberOne + randomNumberTwo == 6){ //if sum is 6
              if ( randomNumberOne == randomNumberTwo){ // 3+3
                 System.out.println("Hard Six");// slang for 3+3
              } 
              else { // if sum 6 not by 3+3
              System.out.println("Easy Six"); // slang of 6
              }
           } 
           else if (randomNumberOne + randomNumberTwo == 7){ // if sum 7
           System.out.println("Seven Out"); // slang for 7
           }
           else if (randomNumberOne + randomNumberTwo == 8){ // if sum 8
              if ( randomNumberOne == randomNumberTwo){ // sum 8 by 4+4
              System.out.println("Hard Eight"); // slang for 4+4
               }
               else {
                System.out.println("Easy Eight"); // slang for sum 8 not 4 and 4
               }
           } 
           else if (randomNumberOne + randomNumberTwo == 9){ // if sum is 9
           System.out.println("Nine"); // slang for 9
           }
           else if (randomNumberOne + randomNumberTwo == 10){ // if sum 10 
              if ( randomNumberOne == randomNumberTwo){ // if 5+5 
             System.out.println("Hard Ten"); // slang sum of two 5
              }
              else {
               System.out.println("Easy Ten"); // slang sum 10 other way
              }
           } 
           else if (randomNumberOne + randomNumberTwo == 11){ // if sum 11
           System.out.println("Yo-leven"); // slang sum 11
           }
            else{
           System.out.println("Boxcars"); // slang if sum 12
           }
        } // end of type cast 

        if (type==2)  { // start of if
        System.out.print(" First number: ");
        double randomNumberOne = myScanner.nextDouble();
        System.out.print(" Second number: ");
        double randomNumberTwo = myScanner.nextDouble();
         if (randomNumberOne + randomNumberTwo == 2){ // sum is two 
          System.out.println("Snake Eyes"); // slang for two 
          }// end if 
          else if (randomNumberOne + randomNumberTwo == 3){ // if sum is three
              System.out.println("Ace Deuce"); // slang for three
           } // end else if
          else if (randomNumberOne + randomNumberTwo == 4){ // if sum 4
             if ( randomNumberOne == randomNumberTwo){ // if both ramdom number are the same
                System.out.println("Hard Four"); // slang for 2 twos
                }
             else {
              System.out.println("Easy Four"); // slang for four when not 2+2
              }
           }
           else if (randomNumberOne + randomNumberTwo == 5){ // if sum is 5
               System.out.println("Fever Five"); // slang for 5 
          }
           else if (randomNumberOne + randomNumberTwo == 6){ //if sum is 6
              if ( randomNumberOne == randomNumberTwo){ // 3+3
                 System.out.println("Hard Six");// slang for 3+3
              } 
              else { // if sum 6 not by 3+3
              System.out.println("Easy Six"); // slang of 6
              }
           } 
           else if (randomNumberOne + randomNumberTwo == 7){ // if sum 7
           System.out.println("Seven Out"); // slang for 7
           }
           else if (randomNumberOne + randomNumberTwo == 8){ // if sum 8
              if ( randomNumberOne == randomNumberTwo){ // sum 8 by 4+4
              System.out.println("Hard Eight"); // slang for 4+4
               }
               else {
                System.out.println("Easy Eight"); // slang for sum 8 not 4 and 4
               }
           } 
           else if (randomNumberOne + randomNumberTwo == 9){ // if sum is 9
           System.out.println("Nine"); // slang for 9
           }
           else if (randomNumberOne + randomNumberTwo == 10){ // if sum 10 
              if ( randomNumberOne == randomNumberTwo){ // if 5+5 
             System.out.println("Hard Ten"); // slang sum of two 5
              }
              else {
               System.out.println("Easy Ten"); // slang sum 10 other way
              }
           } 
           else if (randomNumberOne + randomNumberTwo == 11){ // if sum 11
           System.out.println("Yo-leven"); // slang sum 11
           }
            else{
           System.out.println("Boxcars"); // slang if sum 12
           }
        }// end evaluate

  }// end of main method
}// end of class