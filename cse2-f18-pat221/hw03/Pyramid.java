// Paula Torrebiarte
// 18/09/18
// CSE 2
// Pyramid
// Finds the volume of a pyramid 

import java.util.Scanner; // import sncanner
public class Pyramid{ // opens class
  public static void main(String [] args){ // opens main method
    Scanner myScanner = new Scanner(System.in); // declare instance of object and construct class 
    
    System.out.print("The square side of the pyramid is (input length): "); // inut lenght
      double lenght = myScanner.nextDouble(); // save output as double
    
    System.out.print("The height of the pyramid is (input height):"); //input height
    double height = myScanner.nextDouble(); // save height as double 
     
    // calculations
    double base = lenght * lenght; // area of base using lenght of side
    double volume =  base * height/3; // use volume formula 1/3 * base * height 
    // print volume 
    System.out.println( " The volume inside the pyramid is: " + volume );
    
  }// end of main method
} // end of class