/////
// Paula Torrebiarte
//  09/06/18
// CSE 2 - 311
//Cyclo meter 
// Measure distance, speed
//
public class Cyclometer{

public static void main(String[] args){

// input data
int secsTrip1=480; //seconds trip 1
int secsTrip2=3220; //seconds trip 2
int countsTrip1=1561; //count trip 1
int countsTrip2=9037; // count trip 2

// intermediate variable and output data
double wheelDiameter=27.0; // wheel diameter
double PI=3.14159; // Pi
int feetPerMile=5280; // feet per mile conversion
int inchesPerFoot=12; // inches per foot conversion
int secondsPerMinute=60; // seconds per minute conversion
double distanceTrip1, distanceTrip2, totalDistance;  // establish variables

//print conversions
 System.out.println ("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

//Run the calculations; store the values. Document calculation here. 
		// Calculating distances
		
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
  
  // Print distances
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was " + distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

} // end of main method
} //end of class